import argparse

import numpy as np
import pandas as pd

from typing import List
from instrument.instrument import Instrument
import matplotlib.pyplot as plt


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--args", default="", type=str)
    parser.add_argument("-o", "--output_file", default="visa_instrument.csv", type=str, required=True)
    parser.add_argument("-f1", "--freq_start", default=2.7e9, type=float, required=True)
    parser.add_argument("-f2", "--freq_stop", default=2.8e9, type=float, required=True)
    parser.add_argument("-s", "--freq_step", default=10e6, type=float, required=True)
    parser.add_argument("-r", "--resource", default="@sim", type=str)
    parser.add_argument("-n", "--name", default="ASRL1::INSTR", help="")
    return parser.parse_args()


def print_result(x, y):
    fig = plt.figure(figsize=(11, 6))
    plt.plot(x*1e-6, y, "ko--", label="meas. ampl.")
    plt.xlabel('frequency $f$ [MHz]')
    plt.ylabel('Magnitude [dBr]')
    plt.legend(fontsize=14, loc="lower right")
    plt.xticks(x*1e-6)
    plt.yticks(np.linspace(-25, -85, 13))
    plt.grid()
    plt.show()


def run():
    # parse arguments
    args = parse_args()

    # create Instrument Object for interaction, init Instrument handle
    instr = Instrument(name=args.name,
                       resource=args.resource)
    instr.init_handle(simulate=True)

    # create frequency list based on start parameters
    frequency_list: np.ndarry = np.arange(args.freq_start, args.freq_stop + args.freq_step, args.freq_step)
    amplitude_list: List = []

    # sweep over frequencies, get amplitudes
    for frequency in frequency_list:
        instr.set_frequency(frequency=frequency)
        amplitude = instr.get_amplitude()
        amplitude_list.append(amplitude)

    # print results
    print_result(frequency_list, amplitude_list)

    # create dataframe from freq. and ampl.
    df = pd.DataFrame(list(zip(frequency_list, amplitude_list)), columns=["frequency", "amplitude"])

    # write *.csv as output
    df.to_csv(args.output_file)

    # close Instrument (session)
    instr.close()

if __name__ == "__main__":
    # run as python <Drive>:\visa\examples\main.py -o "visa_instrument.csv" -f1 2.7e9 -f2 2.8e9 -s 10e6
    run()
