import logging
# import attr
import numpy as np

from RsInstrument import RsInstrument, ResourceError


# @attr.s(auto_attribs=True, init=False)
class Instrument(object):
    amplitude: float = -42.0

    def __init__(self, name: str, resource: str):
        self.instrument_name: str = name or "Instrument"
        self.instrument_identifier: str = ""
        self.instrument_handle: RsInstrument = None
        self.instrument_resource: str = resource
        logging.basicConfig(level=logging.INFO,
                            format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
                            datefmt="%m-%d %H:%M",
                            filename=f"{self.instrument_name.replace(':', '')}.log",
                            filemode="a")
        self.instrument_logger = logging.getLogger(f"LOG_{self.instrument_name}")
        self.instrument_logger.info(f"LOG {self.instrument_name}")
        self.instrument_logger.info(f"{self.__repr__()}")

    def __str__(self) -> str:
        return f"Instrument, Name : {self.instrument_name}, ID : {self.instrument_identifier}, " \
               f"Handle : {self.instrument_handle}, IP : {self.instrument_resource}"

    def init_handle(self, id_query: bool = True, reset: bool = False, simulate: bool = False, visa_type: str = "rs",
                    timeout: int = 5000) -> bool:
        ret_val: bool
        try:
            instrument_handle: RsInstrument = RsInstrument(f"{self.instrument_resource}", id_query, reset,
                                                           f"Simulate={str(simulate)}, SelectVisa='{visa_type}'")
            instrument_handle.opc_query_after_write = True  # avoid manual use of *OPC
            instrument_handle.visa_timeout = timeout  # zb 5000 in ms
            self.instrument_logger.info(f"Instrument options: {','.join(instrument_handle.instrument_options)}")
            self.instrument_handle = instrument_handle
            self.instrument_logger.info("Init Instrument handle success!")
            ret_val = True
        except ResourceError as e:
            self.instrument_logger.exception(f"{e.args[0]}")
            self.instrument_logger.error("ERROR cant create instrument handle")
            del self.instrument_handle
            ret_val = False
        return ret_val

    def get_identifier(self):
        assert self.instrument_handle
        self.instrument_identifier = self.instrument_handle.query_str("*IDN?")
        self.instrument_logger.info(f"Retrieved Identifier of {self.instrument_name} : {self.instrument_identifier}")

    def close(self):
        self.instrument_handle.close()
        del self.instrument_handle
        self.instrument_logger.info(f"Closed connection with Instrument '{self.instrument_name}'"
                                    f" at {self.instrument_resource}")

    def set_default_setup(self):
        assert self.instrument_handle
        self.instrument_logger.info(f"Loading default Setup for {self.instrument_name}")
        self.instrument_handle.clear_status()

    def get_frequency(self) -> float:
        frequency: str = self.instrument_handle.query_str("SENS:FREQ?")
        return float(frequency)

    def set_frequency(self, frequency: float):
        self.instrument_handle.write_str(f"SENS:FREQ {frequency}")
        self.modify_amplitude()

    def get_amplitude(self) -> float:
        # amplitude = self.instrument_handle.query_str("POW:LEV:IMM:AMPL?")
        return self.amplitude

    def modify_amplitude(self):
        self.amplitude = - (np.random.rand() * 50 + 30)
