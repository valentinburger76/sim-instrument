# README Project VISA demonstration

use requirements.txt to initialize the venv
launch ``main.py`` with arguments ``-o "visa_instrument.csv" -f1 2.7e9 -f2 2.8e9 -s 10e6``
for result filename ``"visa_instrument.csv"``, start/stop freq of 2.7 and 2.8 GHz with 10 MHz steps
the ``src`` folder has to be declared as ``sources root`` in pycharm so that the ``ìnstrument`` packet is resolved properly in ``main.py``

program code in ``main.py`` is commented, Instrument class in ``src.instrument.instrument`` is not commented but self explanatory
a basic logger is used that dumps the log into the script path to ``main.py`` (ideally this would land in a separate log folder)
results (*.csv file) too is out of ease simply dumped into the script path folder
the ``src.test`` folder exists for unittests that have not been written yet and will likely never be written (thus is empty)
for unittests the ``unittest`` and explicitly the ``unittest.mock`` (or MagicMock) would be used extensively for proper test code (as is the standard for serious applications)

note that instead of the ``visa`` or ``PyVisa`` class the very similar class ``RsInstrument`` is used instead (provided by R&S GmBh), as it provides more comfortable high level functions that the ``visa`` class
see the [R&S RsInstrument simulate session](https://rsinstrument.readthedocs.io/en/latest/StepByStepGuide.html#simulating-session) respective docpage which is the replacement for the suggested approach of ``pyvisa-sim`` in the original task


*END*
